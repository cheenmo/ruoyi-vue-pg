package com.ruoyi.web.controller.common;

import com.ruoyi.common.config.RuoYiConfig;
import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.file.FileUploadUtils;
import com.ruoyi.common.utils.file.FileUtils;
import com.ruoyi.framework.config.ServerConfig;
import com.ruoyi.framework.web.service.TokenService;
import com.ruoyi.system.domain.SysAppendix;
import com.ruoyi.system.service.ISysAppendixService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.Date;

/**
 * 通用请求处理
 * 
 * @author ruoyi
 */
@RestController
@Api(tags={"【文件上传下载】Controller"})
public class CommonController
{
    private static final Logger log = LoggerFactory.getLogger(CommonController.class);

    @Autowired
    private ServerConfig serverConfig;

    @Autowired
    private ISysAppendixService sysAppendixService;

    @Autowired
    private TokenService tokenService;

    /**
     * 通用下载请求
     * 
     * @param fileName 文件名称
     * @param delete 是否删除
     */
    @GetMapping("common/download")
    @ApiOperation("文件下载")
    public void fileDownload(String fileName, Boolean delete, HttpServletResponse response)
    {
        try
        {
            if (!FileUtils.checkAllowDownload(fileName))
            {
                throw new Exception(StringUtils.format("文件名称({})非法，不允许下载。 ", fileName));
            }
            String realFileName = System.currentTimeMillis() + fileName.substring(fileName.indexOf("_") + 1);
            String filePath = RuoYiConfig.getDownloadPath() + fileName;

            response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
            FileUtils.setAttachmentResponseHeader(response, realFileName);
            FileUtils.writeBytes(filePath, response.getOutputStream());
            if (delete)
            {
                FileUtils.deleteFile(filePath);
            }
        }
        catch (Exception e)
        {
            log.error("下载文件失败", e);
        }
    }

    /**
     * 通用上传请求
     */
    @PostMapping("/common/upload")
    @ApiOperation("文件上传")
    public AjaxResult upload(@RequestPart(value="file") MultipartFile file) {
        try
        {

            // 上传并返回新文件名称
            String fileName = FileUploadUtils.upload(RuoYiConfig.getUploadPath(), file);
            String url = serverConfig.getUrl() + fileName;
            AjaxResult ajax = AjaxResult.success();

            ajax.put("fileName", fileName);
            ajax.put("url", url);

            return ajax;
        }
        catch (Exception e)
        {
            return AjaxResult.error(e.getMessage());
        }
    }

    /**
     * 通用上传请求
     */
    @PostMapping("/common/uploadFile")
    @ApiOperation("文件上传")
    public AjaxResult uploadFile(@RequestPart(value="file") MultipartFile file)
    {

        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        SysUser user = loginUser.getUser();
        try
        {

            // 上传并返回新文件名称
            String filePath = FileUploadUtils.upload(RuoYiConfig.getUploadPath(), file);
            // 上传前的文件名
            String fileDesc = file.getOriginalFilename();
            // 上传后的文件名
            String fileName = filePath.substring(filePath.lastIndexOf("/")+1);
            // 获取后缀
            String suffixName = fileDesc.substring(fileDesc.lastIndexOf("."));

            SysAppendix sysAppendix = new SysAppendix();
            sysAppendix.setAppendixDesc(file.getOriginalFilename());
            sysAppendix.setAppendixName(fileName);
            sysAppendix.setAppendixSuffix(suffixName);
            sysAppendix.setAppendixSize(String.valueOf(file.getSize()));
            sysAppendix.setAppendixPath(filePath);
            sysAppendix.setCreateBy(user.getUserName());
            sysAppendix.setCreateTime(new Date());


            return AjaxResult.success(sysAppendixService.insertSysAppendix(sysAppendix));
        }
        catch (Exception e)
        {
            return AjaxResult.error(e.getMessage());
        }
    }

    /**
     * 本地资源通用下载
     */
    @GetMapping("/common/download/resource")
    public void resourceDownload(String resource, HttpServletResponse response) {
        try
        {
            if (!FileUtils.checkAllowDownload(resource))
            {
                throw new Exception(StringUtils.format("资源文件({})非法，不允许下载。 ", resource));
            }
            // 本地资源路径
            String localPath = RuoYiConfig.getProfile();
            // 数据库资源地址
            String downloadPath = localPath + StringUtils.substringAfter(resource, Constants.RESOURCE_PREFIX);
            // 下载名称
            String downloadName = StringUtils.substringAfterLast(downloadPath, "/");
            response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
            FileUtils.setAttachmentResponseHeader(response, downloadName);
            FileUtils.writeBytes(downloadPath, response.getOutputStream());
        }
        catch (Exception e)
        {
            log.error("下载文件失败", e);
        }
    }
}
