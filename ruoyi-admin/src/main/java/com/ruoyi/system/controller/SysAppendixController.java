package com.ruoyi.system.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.SysAppendix;
import com.ruoyi.system.service.ISysAppendixService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 附件Controller
 * 
 * @author 尘墨
 * @date 2021-08-13
 */
@RestController
@RequestMapping("/system/appendix")
@Api("【附件】Controller")
public class SysAppendixController extends BaseController
{
    @Autowired
    private ISysAppendixService sysAppendixService;

    /**
     * 查询附件列表
     */
    @PreAuthorize("@ss.hasPermi('system:appendix:list')")
    @ApiOperation("查询附件列表")
    @GetMapping("/list")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "pageNum",value = "当前页码" ,dataType = "int",dataTypeClass=Integer.class,paramType = "query"),
        @ApiImplicitParam(name = "pageSize",value = "每页数据量" , dataType = "int",dataTypeClass=Integer.class, paramType = "query"),
    })
    public TableDataInfo list(SysAppendix sysAppendix)
    {
        startPage();
        List<SysAppendix> list = sysAppendixService.selectSysAppendixList(sysAppendix);
        return getDataTable(list);
    }

    /**
     * 导出附件列表
     */
    @PreAuthorize("@ss.hasPermi('system:appendix:export')")
    @ApiOperation("导出附件列表")
    @Log(title = "附件", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SysAppendix sysAppendix)
    {
        List<SysAppendix> list = sysAppendixService.selectSysAppendixList(sysAppendix);
        ExcelUtil<SysAppendix> util = new ExcelUtil<SysAppendix>(SysAppendix.class);
        return util.exportExcel(list, "附件数据");
    }

    /**
     * 获取附件详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:appendix:query')")
    @ApiOperation("获取附件信息")
    @ApiImplicitParam(name = "appendixId", value = "附件主键", required = true, dataType = "Long",dataTypeClass=Long.class, paramType = "path")
    @GetMapping(value = "/{appendixId}")
    public AjaxResult getInfo(@PathVariable("appendixId") Long appendixId)
    {
        return AjaxResult.success(sysAppendixService.selectSysAppendixByAppendixId(appendixId));
    }

    /**
     * 新增附件
     */
    @PreAuthorize("@ss.hasPermi('system:appendix:add')")
    @ApiOperation("新增附件信息")
    @Log(title = "附件", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysAppendix sysAppendix)
    {
        return toAjax(sysAppendixService.insertSysAppendix(sysAppendix));
    }

    /**
     * 修改附件
     */
    @PreAuthorize("@ss.hasPermi('system:appendix:edit')")
    @ApiOperation("修改附件信息")
    @Log(title = "附件", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysAppendix sysAppendix)
    {
        return toAjax(sysAppendixService.updateSysAppendix(sysAppendix));
    }

    /**
     * 删除附件
     */
    @PreAuthorize("@ss.hasPermi('system:appendix:remove')")
    @ApiOperation("删除附件信息")
    @ApiImplicitParam(name = "appendixId", value = "附件主键", required = true, dataType = "Long",dataTypeClass=Long.class, paramType = "path")
    @Log(title = "附件", businessType = BusinessType.DELETE)
    @DeleteMapping("/{appendixIds}")
    public AjaxResult remove(@PathVariable Long[] appendixIds)
    {
        return toAjax(sysAppendixService.deleteSysAppendixByAppendixIds(appendixIds));
    }
}
