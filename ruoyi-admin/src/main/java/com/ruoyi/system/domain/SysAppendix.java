package com.ruoyi.system.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 附件对象 sys_appendix
 * 
 * @author 尘墨
 * @date 2021-08-13
 */

@ApiModel(value = "附件对象")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SysAppendix extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 附件ID */
    @ApiModelProperty(value = "附件ID",position = 0)
    private Long appendixId;

    /** 文件名 */
    @Excel(name = "文件名")
    @ApiModelProperty(value = "文件名",position = 1)
    private String appendixDesc;

    /** 附件名 */
    @Excel(name = "附件名")
    @ApiModelProperty(value = "附件名",position = 2)
    private String appendixName;

    /** 后缀名 */
    @Excel(name = "后缀名")
    @ApiModelProperty(value = "后缀名",position = 3)
    private String appendixSuffix;

    /** 大小KB */
    @Excel(name = "大小KB")
    @ApiModelProperty(value = "大小KB",position = 4)
    private String appendixSize;

    /** 路径 */
    @Excel(name = "路径")
    @ApiModelProperty(value = "路径",position = 5)
    private String appendixPath;

    /** 删除 */
    @ApiModelProperty(value = "删除",position = 6)
    private Long delFlag;

    public void setAppendixId(Long appendixId) 
    {
        this.appendixId = appendixId;
    }

    public Long getAppendixId() 
    {
        return appendixId;
    }
    public void setAppendixDesc(String appendixDesc) 
    {
        this.appendixDesc = appendixDesc;
    }

    public String getAppendixDesc() 
    {
        return appendixDesc;
    }
    public void setAppendixName(String appendixName) 
    {
        this.appendixName = appendixName;
    }

    public String getAppendixName() 
    {
        return appendixName;
    }
    public void setAppendixSuffix(String appendixSuffix) 
    {
        this.appendixSuffix = appendixSuffix;
    }

    public String getAppendixSuffix() 
    {
        return appendixSuffix;
    }
    public void setAppendixSize(String appendixSize) 
    {
        this.appendixSize = appendixSize;
    }

    public String getAppendixSize() 
    {
        return appendixSize;
    }
    public void setAppendixPath(String appendixPath) 
    {
        this.appendixPath = appendixPath;
    }

    public String getAppendixPath() 
    {
        return appendixPath;
    }
    public void setDelFlag(Long delFlag) 
    {
        this.delFlag = delFlag;
    }

    public Long getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("appendixId", getAppendixId())
            .append("appendixDesc", getAppendixDesc())
            .append("appendixName", getAppendixName())
            .append("appendixSuffix", getAppendixSuffix())
            .append("appendixSize", getAppendixSize())
            .append("appendixPath", getAppendixPath())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
