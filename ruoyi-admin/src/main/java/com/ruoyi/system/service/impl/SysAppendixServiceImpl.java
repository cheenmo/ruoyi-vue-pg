package com.ruoyi.system.service.impl;

import com.ruoyi.common.config.RuoYiConfig;
import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.file.FileUtils;
import com.ruoyi.system.domain.SysAppendix;
import com.ruoyi.system.mapper.SysAppendixMapper;
import com.ruoyi.system.service.ISysAppendixService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 附件Service业务层处理
 * 
 * @author 尘墨
 * @date 2021-08-13
 */
@Service
public class SysAppendixServiceImpl implements ISysAppendixService 
{
    @Autowired
    private SysAppendixMapper sysAppendixMapper;

    /**
     * 查询附件
     * 
     * @param appendixId 附件主键
     * @return 附件
     */
    @Override
    public SysAppendix selectSysAppendixByAppendixId(Long appendixId)
    {
        return sysAppendixMapper.selectSysAppendixByAppendixId(appendixId);
    }

    /**
     * 查询附件列表
     * 
     * @param sysAppendix 附件
     * @return 附件
     */
    @Override
    public List<SysAppendix> selectSysAppendixList(SysAppendix sysAppendix)
    {
        return sysAppendixMapper.selectSysAppendixList(sysAppendix);
    }

    /**
     * 新增附件
     * 
     * @param sysAppendix 附件
     * @return 结果
     */
    @Override
    public int insertSysAppendix(SysAppendix sysAppendix)
    {
        sysAppendix.setCreateTime(DateUtils.getNowDate());
        return sysAppendixMapper.insertSysAppendix(sysAppendix);
    }

    /**
     * 修改附件
     * 
     * @param sysAppendix 附件
     * @return 结果
     */
    @Override
    public int updateSysAppendix(SysAppendix sysAppendix)
    {
        sysAppendix.setUpdateTime(DateUtils.getNowDate());
        return sysAppendixMapper.updateSysAppendix(sysAppendix);
    }

    /**
     * 批量删除附件
     * 
     * @param appendixIds 需要删除的附件主键
     * @return 结果
     */
    @Override
    public int deleteSysAppendixByAppendixIds(Long[] appendixIds)
    {
        //删除服务器中的文件
        for (Long appendixId : appendixIds) {
            SysAppendix appendix = sysAppendixMapper.selectSysAppendixByAppendixId(appendixId);
            if(appendix !=null){
                String absPath = RuoYiConfig.getProfile()+ appendix.getAppendixPath().replace(Constants.RESOURCE_PREFIX,"");
                FileUtils.deleteFile(absPath);
            }
        }
        return sysAppendixMapper.deleteSysAppendixByAppendixIds(appendixIds);
    }

    /**
     * 删除附件信息
     * 
     * @param appendixId 附件主键
     * @return 结果
     */
    @Override
    public int deleteSysAppendixByAppendixId(Long appendixId)
    {
        SysAppendix appendix = sysAppendixMapper.selectSysAppendixByAppendixId(appendixId);
        if(appendix !=null){
            String absPath = RuoYiConfig.getProfile()+ appendix.getAppendixPath().replace(Constants.RESOURCE_PREFIX,"");
            FileUtils.deleteFile(absPath);
        }
        return sysAppendixMapper.deleteSysAppendixByAppendixId(appendixId);
    }
}
