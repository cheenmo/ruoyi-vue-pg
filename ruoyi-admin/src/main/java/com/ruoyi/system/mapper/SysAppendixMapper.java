package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.SysAppendix;

import java.util.List;

/**
 * 附件Mapper接口
 * 
 * @author 尘墨
 * @date 2021-08-13
 */
public interface SysAppendixMapper 
{
    /**
     * 查询附件
     * 
     * @param appendixId 附件主键
     * @return 附件
     */
    SysAppendix selectSysAppendixByAppendixId(Long appendixId);

    /**
     * 查询附件列表
     * 
     * @param sysAppendix 附件
     * @return 附件集合
     */
    List<SysAppendix> selectSysAppendixList(SysAppendix sysAppendix);

    /**
     * 新增附件
     * 
     * @param sysAppendix 附件
     * @return 结果
     */
    int insertSysAppendix(SysAppendix sysAppendix);

    /**
     * 修改附件
     * 
     * @param sysAppendix 附件
     * @return 结果
     */
    int updateSysAppendix(SysAppendix sysAppendix);

    /**
     * 删除附件
     * 
     * @param appendixId 附件主键
     * @return 结果
     */
    int deleteSysAppendixByAppendixId(Long appendixId);

    /**
     * 批量删除附件
     * 
     * @param appendixIds 需要删除的数据主键集合
     * @return 结果
     */
    int deleteSysAppendixByAppendixIds(Long[] appendixIds);
}
