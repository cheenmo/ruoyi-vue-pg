
DROP TABLE IF EXISTS "sys_appendix";
CREATE TABLE "sys_appendix" (
    "appendix_id" BIGSERIAL NOT NULL,
    "appendix_desc" varchar(1000),
    "appendix_name" varchar(500),
    "appendix_suffix" varchar(20),
    "appendix_size" varchar(20),
    "appendix_path" varchar(1000),
    "del_flag" int4 DEFAULT 0,
    "create_by" varchar(64),
    "create_time" timestamp(6) DEFAULT now(),
    "update_by" varchar(64),
    "update_time" timestamp(6),
    "remark" varchar(500),
    PRIMARY KEY ("appendix_id")
);
COMMENT ON COLUMN "sys_appendix"."appendix_id" IS '附件ID';
COMMENT ON COLUMN "sys_appendix"."appendix_desc" IS '显示名(原文件名)';
COMMENT ON COLUMN "sys_appendix"."appendix_name" IS '附件名';
COMMENT ON COLUMN "sys_appendix"."appendix_suffix" IS '附件后缀名';
COMMENT ON COLUMN "sys_appendix"."appendix_size" IS '附件大小KB';
COMMENT ON COLUMN "sys_appendix"."appendix_path" IS '附件路径';
COMMENT ON COLUMN "sys_appendix"."del_flag" IS '删除标识(0 正常 2删除)';
COMMENT ON COLUMN "sys_appendix"."create_by" IS '创建者';
COMMENT ON COLUMN "sys_appendix"."create_time" IS '创建时间';
COMMENT ON COLUMN "sys_appendix"."update_by" IS '更新人';
COMMENT ON COLUMN "sys_appendix"."update_time" IS '更新时间';
COMMENT ON COLUMN "sys_appendix"."remark" IS '备注';
COMMENT ON TABLE "sys_appendix" IS '附件信息';

alter sequence if exists sys_appendix_appendix_id_seq cache 10;

INSERT INTO sys_dict_type VALUES (11, '是否删除', 'sys_del_flag', '0', 'admin', now(), '', NULL, '删除状态');
INSERT INTO sys_dict_data VALUES (29, 1, '正常', '0', 'sys_del_flag', '', 'primary', 'N', '0', 'admin', now(), '', NULL, '正常');
INSERT INTO sys_dict_data VALUES (30, 2, '已删', '2', 'sys_del_flag', '', 'danger', 'N', '0', 'admin', now(), '', NULL, '已删');

-- 菜单 SQL
do
$$
    declare parentId bigint;
    begin
        insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame,is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
        values('附件管理', '1', '10', 'appendix', 'system/appendix/index', 1,0, 'C', '0', '0', 'system:appendix:list', 'upload', 'admin', now(),'', now(), '附件菜单') returning menu_id into parentId;

-- 按钮 SQL
        insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame,is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
        values('附件查询', parentId, '1',  '#', '', 1,0,  'F', '0',  '0', 'system:appendix:query','#', 'admin', now(),'', now(), '');

        insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame,is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
        values('附件新增', parentId, '2',  '#', '', 1,0,  'F', '0',  '0', 'system:appendix:add',  '#', 'admin', now(),'', now(), '');

        insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame,is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
        values('附件修改', parentId, '3',  '#', '', 1,0,  'F', '0',  '0', 'system:appendix:edit', '#', 'admin', now(),'', now(), '');

        insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame,is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
        values('附件删除', parentId, '4',  '#', '', 1,0,  'F', '0',  '0', 'system:appendix:remove','#', 'admin', now(),'', now(), '');

        insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame,is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
        values('附件导出', parentId, '5',  '#', '', 1,0,  'F', '0',  '0', 'system:appendix:export','#', 'admin', now(),'', now(), '');

    end
$$;