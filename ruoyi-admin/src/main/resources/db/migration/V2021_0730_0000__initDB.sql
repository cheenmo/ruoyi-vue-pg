
-- ----------------------------
-- Table structure for gen_table
-- ----------------------------
DROP TABLE IF EXISTS gen_table;
CREATE TABLE gen_table (
  table_id BIGSERIAL NOT NULL,
  table_name varchar(200) DEFAULT '',
  table_comment varchar(500) DEFAULT '',
  sub_table_name varchar(64) ,
  sub_table_fk_name varchar(64) ,
  class_name varchar(100) DEFAULT '',
  tpl_category varchar(200) DEFAULT 'crud',
  package_name varchar(100) ,
  module_name varchar(30) ,
  business_name varchar(30) ,
  function_name varchar(50) ,
  function_author varchar(50) ,
  gen_type char(1) DEFAULT '0',
  gen_path varchar(200) DEFAULT '/',
  options varchar(1000) ,
  create_by varchar(64) DEFAULT '',
  create_time timestamp(6),
  update_by varchar(64) DEFAULT '',
  update_time timestamp(6),
  remark varchar(500) ,
  CONSTRAINT gen_table_pkey PRIMARY KEY (table_id)
);
COMMENT ON COLUMN gen_table.table_id IS '编号';
COMMENT ON COLUMN gen_table.table_name IS '表名称';
COMMENT ON COLUMN gen_table.table_comment IS '表描述';
COMMENT ON COLUMN gen_table.sub_table_name IS '关联子表的表名';
COMMENT ON COLUMN gen_table.sub_table_fk_name IS '子表关联的外键名';
COMMENT ON COLUMN gen_table.class_name IS '实体类名称';
COMMENT ON COLUMN gen_table.tpl_category IS '使用的模板（crud单表操作 tree树表操作）';
COMMENT ON COLUMN gen_table.package_name IS '生成包路径';
COMMENT ON COLUMN gen_table.module_name IS '生成模块名';
COMMENT ON COLUMN gen_table.business_name IS '生成业务名';
COMMENT ON COLUMN gen_table.function_name IS '生成功能名';
COMMENT ON COLUMN gen_table.function_author IS '生成功能作者';
COMMENT ON COLUMN gen_table.gen_type IS '生成代码方式（0zip压缩包 1自定义路径）';
COMMENT ON COLUMN gen_table.gen_path IS '生成路径（不填默认项目路径）';
COMMENT ON COLUMN gen_table.options IS '其它生成选项';
COMMENT ON COLUMN gen_table.create_by IS '创建者';
COMMENT ON COLUMN gen_table.create_time IS '创建时间';
COMMENT ON COLUMN gen_table.update_by IS '更新者';
COMMENT ON COLUMN gen_table.update_time IS '更新时间';
COMMENT ON COLUMN gen_table.remark IS '备注';
COMMENT ON TABLE gen_table IS '代码生成业务表';

-- ----------------------------
-- Records of gen_table
-- ----------------------------

-- ----------------------------
-- Table structure for gen_table_column
-- ----------------------------
DROP TABLE IF EXISTS gen_table_column;
CREATE TABLE gen_table_column (
  column_id BIGSERIAL NOT NULL,
  table_id varchar(64) ,
  column_name varchar(200) ,
  column_comment varchar(500) ,
  column_type varchar(100) ,
  java_type varchar(500) ,
  java_field varchar(200) ,
  is_pk char(1) ,
  is_increment char(1) ,
  is_required char(1) ,
  is_insert char(1) ,
  is_edit char(1) ,
  is_list char(1) ,
  is_query char(1) ,
  query_type varchar(200) DEFAULT 'EQ',
  html_type varchar(200) ,
  dict_type varchar(200) DEFAULT '',
  sort int4,
  create_by varchar(64) DEFAULT '',
  create_time timestamp(6),
  update_by varchar(64) DEFAULT '',
  update_time timestamp(6) ,
  CONSTRAINT gen_table_column_pkey PRIMARY KEY (column_id)
);
COMMENT ON COLUMN gen_table_column.column_id IS '编号';
COMMENT ON COLUMN gen_table_column.table_id IS '归属表编号';
COMMENT ON COLUMN gen_table_column.column_name IS '列名称';
COMMENT ON COLUMN gen_table_column.column_comment IS '列描述';
COMMENT ON COLUMN gen_table_column.column_type IS '列类型';
COMMENT ON COLUMN gen_table_column.java_type IS 'JAVA类型';
COMMENT ON COLUMN gen_table_column.java_field IS 'JAVA字段名';
COMMENT ON COLUMN gen_table_column.is_pk IS '是否主键（1是）';
COMMENT ON COLUMN gen_table_column.is_increment IS '是否自增（1是）';
COMMENT ON COLUMN gen_table_column.is_required IS '是否必填（1是）';
COMMENT ON COLUMN gen_table_column.is_insert IS '是否为插入字段（1是）';
COMMENT ON COLUMN gen_table_column.is_edit IS '是否编辑字段（1是）';
COMMENT ON COLUMN gen_table_column.is_list IS '是否列表字段（1是）';
COMMENT ON COLUMN gen_table_column.is_query IS '是否查询字段（1是）';
COMMENT ON COLUMN gen_table_column.query_type IS '查询方式（等于、不等于、大于、小于、范围）';
COMMENT ON COLUMN gen_table_column.html_type IS '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）';
COMMENT ON COLUMN gen_table_column.dict_type IS '字典类型';
COMMENT ON COLUMN gen_table_column.sort IS '排序';
COMMENT ON COLUMN gen_table_column.create_by IS '创建者';
COMMENT ON COLUMN gen_table_column.create_time IS '创建时间';
COMMENT ON COLUMN gen_table_column.update_by IS '更新者';
COMMENT ON COLUMN gen_table_column.update_time IS '更新时间';
COMMENT ON TABLE gen_table_column IS '代码生成业务表字段';

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS sys_config;
CREATE TABLE sys_config (
  config_id BIGSERIAL NOT NULL,
  config_name varchar(100) DEFAULT '0',
  config_key varchar(100) DEFAULT '',
  config_value varchar(500) DEFAULT '',
  config_type char(1) DEFAULT '',
  create_by varchar(64) DEFAULT '',
  create_time timestamp(6),
  update_by varchar(64) DEFAULT '',
  update_time timestamp(6),
  remark varchar(500) ,
  CONSTRAINT sys_config_pkey PRIMARY KEY (config_id)
)
;
COMMENT ON COLUMN sys_config.config_id IS '参数主键';
COMMENT ON COLUMN sys_config.config_name IS '参数名称';
COMMENT ON COLUMN sys_config.config_key IS '参数键名';
COMMENT ON COLUMN sys_config.config_value IS '参数键值';
COMMENT ON COLUMN sys_config.config_type IS '系统内置（Y是 N否）';
COMMENT ON COLUMN sys_config.create_by IS '创建者';
COMMENT ON COLUMN sys_config.create_time IS '创建时间';
COMMENT ON COLUMN sys_config.update_by IS '更新者';
COMMENT ON COLUMN sys_config.update_time IS '更新时间';
COMMENT ON COLUMN sys_config.remark IS '备注';
COMMENT ON TABLE sys_config IS '参数配置表';

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO sys_config VALUES (1, '主框架页-默认皮肤样式名称', 'sys.index.skinName', 'skin-blue', 'Y', 'admin', now(), '', NULL, '蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow');
INSERT INTO sys_config VALUES (2, '用户管理-账号初始密码', 'sys.user.initPassword', '123456', 'Y', 'admin', now(), '', NULL, '初始化密码 123456');
INSERT INTO sys_config VALUES (3, '主框架页-侧边栏主题', 'sys.index.sideTheme', 'theme-dark', 'Y', 'admin', now(), '', NULL, '深色主题theme-dark，浅色主题theme-light');
INSERT INTO sys_config VALUES (4, '账号自助-验证码开关', 'sys.account.captchaOnOff', 'true', 'Y', 'admin', now(), '', NULL, '是否开启登录验证码功能（true开启，false关闭）');
insert into sys_config values(5, '账号自助-是否开启用户注册功能', 'sys.account.registerUser', 'true', 'Y', 'admin', now(), '', null, '是否开启注册用户功能（true开启，false关闭）');

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS sys_dept;
CREATE TABLE sys_dept (
  dept_id BIGSERIAL NOT NULL,
  parent_id int8 DEFAULT 0,
  ancestors varchar(50) ,
  dept_name varchar(30) DEFAULT '',
  order_num int4 DEFAULT 0,
  leader varchar(20) DEFAULT '',
  phone varchar(11) DEFAULT '',
  email varchar(50) DEFAULT '',
  status char(1) DEFAULT '0',
  del_flag char(1) DEFAULT '0',
  create_by varchar(64) DEFAULT '',
  create_time timestamp(6),
  update_by varchar(64) DEFAULT '',
  update_time timestamp(6) ,
  CONSTRAINT sys_dept_pkey PRIMARY KEY (dept_id)
);
COMMENT ON COLUMN sys_dept.dept_id IS '部门id';
COMMENT ON COLUMN sys_dept.parent_id IS '父部门id';
COMMENT ON COLUMN sys_dept.ancestors IS '祖级列表';
COMMENT ON COLUMN sys_dept.dept_name IS '部门名称';
COMMENT ON COLUMN sys_dept.order_num IS '显示顺序';
COMMENT ON COLUMN sys_dept.leader IS '负责人';
COMMENT ON COLUMN sys_dept.phone IS '联系电话';
COMMENT ON COLUMN sys_dept.email IS '邮箱';
COMMENT ON COLUMN sys_dept.status IS '部门状态（0正常 1停用）';
COMMENT ON COLUMN sys_dept.del_flag IS '删除标志（0代表存在 2代表删除）';
COMMENT ON COLUMN sys_dept.create_by IS '创建者';
COMMENT ON COLUMN sys_dept.create_time IS '创建时间';
COMMENT ON COLUMN sys_dept.update_by IS '更新者';
COMMENT ON COLUMN sys_dept.update_time IS '更新时间';
COMMENT ON TABLE sys_dept IS '部门表';

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO sys_dept VALUES (100, 0, '0', '若依科技', 0, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', now(), '', NULL);
INSERT INTO sys_dept VALUES (101, 100, '0,100', '深圳总公司', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', now(), '', NULL);
INSERT INTO sys_dept VALUES (102, 100, '0,100', '长沙分公司', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', now(), '', NULL);
INSERT INTO sys_dept VALUES (103, 101, '0,100,101', '研发部门', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', now(), '', NULL);
INSERT INTO sys_dept VALUES (104, 101, '0,100,101', '市场部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', now(), '', NULL);
INSERT INTO sys_dept VALUES (105, 101, '0,100,101', '测试部门', 3, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', now(), '', NULL);
INSERT INTO sys_dept VALUES (106, 101, '0,100,101', '财务部门', 4, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', now(), '', NULL);
INSERT INTO sys_dept VALUES (107, 101, '0,100,101', '运维部门', 5, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', now(), '', NULL);
INSERT INTO sys_dept VALUES (108, 102, '0,100,102', '市场部门', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', now(), '', NULL);
INSERT INTO sys_dept VALUES (109, 102, '0,100,102', '财务部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', now(), '', NULL);

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS sys_dict_data;
CREATE TABLE sys_dict_data (
  dict_code BIGSERIAL NOT NULL,
  dict_sort int4 DEFAULT 0,
  dict_label varchar(100) DEFAULT '',
  dict_value varchar(100) DEFAULT '',
  dict_type varchar(100) DEFAULT '',
  css_class varchar(100) ,
  list_class varchar(100) ,
  is_DEFAULT char(1) ,
  status char(1) DEFAULT '0',
  create_by varchar(64) DEFAULT '',
  create_time timestamp(6),
  update_by varchar(64) DEFAULT '',
  update_time timestamp(6),
  remark varchar(500) ,
  CONSTRAINT sys_dict_data_pkey PRIMARY KEY (dict_code)
)
;
COMMENT ON COLUMN sys_dict_data.dict_code IS '字典编码';
COMMENT ON COLUMN sys_dict_data.dict_sort IS '字典排序';
COMMENT ON COLUMN sys_dict_data.dict_label IS '字典标签';
COMMENT ON COLUMN sys_dict_data.dict_value IS '字典键值';
COMMENT ON COLUMN sys_dict_data.dict_type IS '字典类型';
COMMENT ON COLUMN sys_dict_data.css_class IS '样式属性（其他样式扩展）';
COMMENT ON COLUMN sys_dict_data.list_class IS '表格回显样式';
COMMENT ON COLUMN sys_dict_data.is_DEFAULT IS '是否默认（Y是 N否）';
COMMENT ON COLUMN sys_dict_data.status IS '状态（0正常 1停用）';
COMMENT ON COLUMN sys_dict_data.create_by IS '创建者';
COMMENT ON COLUMN sys_dict_data.create_time IS '创建时间';
COMMENT ON COLUMN sys_dict_data.update_by IS '更新者';
COMMENT ON COLUMN sys_dict_data.update_time IS '更新时间';
COMMENT ON COLUMN sys_dict_data.remark IS '备注';
COMMENT ON TABLE sys_dict_data IS '字典数据表';

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO sys_dict_data VALUES (1, 1, '男', '0', 'sys_user_sex', '', '', 'Y', '0', 'admin', now(), '', NULL, '性别男');
INSERT INTO sys_dict_data VALUES (2, 2, '女', '1', 'sys_user_sex', '', '', 'N', '0', 'admin', now(), '', NULL, '性别女');
INSERT INTO sys_dict_data VALUES (3, 3, '未知', '2', 'sys_user_sex', '', '', 'N', '0', 'admin', now(), '', NULL, '性别未知');
INSERT INTO sys_dict_data VALUES (4, 1, '显示', '0', 'sys_show_hide', '', 'primary', 'Y', '0', 'admin', now(), '', NULL, '显示菜单');
INSERT INTO sys_dict_data VALUES (5, 2, '隐藏', '1', 'sys_show_hide', '', 'danger', 'N', '0', 'admin', now(), '', NULL, '隐藏菜单');
INSERT INTO sys_dict_data VALUES (6, 1, '正常', '0', 'sys_normal_disable', '', 'primary', 'Y', '0', 'admin', now(), '', NULL, '正常状态');
INSERT INTO sys_dict_data VALUES (7, 2, '停用', '1', 'sys_normal_disable', '', 'danger', 'N', '0', 'admin', now(), '', NULL, '停用状态');
INSERT INTO sys_dict_data VALUES (8, 1, '正常', '0', 'sys_job_status', '', 'primary', 'Y', '0', 'admin', now(), '', NULL, '正常状态');
INSERT INTO sys_dict_data VALUES (9, 2, '暂停', '1', 'sys_job_status', '', 'danger', 'N', '0', 'admin', now(), '', NULL, '停用状态');
INSERT INTO sys_dict_data VALUES (10, 1, '默认', 'DEFAULT', 'sys_job_group', '', '', 'Y', '0', 'admin', now(), '', NULL, '默认分组');
INSERT INTO sys_dict_data VALUES (11, 2, '系统', 'SYSTEM', 'sys_job_group', '', '', 'N', '0', 'admin', now(), '', NULL, '系统分组');
INSERT INTO sys_dict_data VALUES (12, 1, '是', 'Y', 'sys_yes_no', '', 'primary', 'Y', '0', 'admin', now(), '', NULL, '系统默认是');
INSERT INTO sys_dict_data VALUES (13, 2, '否', 'N', 'sys_yes_no', '', 'danger', 'N', '0', 'admin', now(), '', NULL, '系统默认否');
INSERT INTO sys_dict_data VALUES (14, 1, '通知', '1', 'sys_notice_type', '', 'warning', 'Y', '0', 'admin', now(), '', NULL, '通知');
INSERT INTO sys_dict_data VALUES (15, 2, '公告', '2', 'sys_notice_type', '', 'success', 'N', '0', 'admin', now(), '', NULL, '公告');
INSERT INTO sys_dict_data VALUES (16, 1, '正常', '0', 'sys_notice_status', '', 'primary', 'Y', '0', 'admin', now(), '', NULL, '正常状态');
INSERT INTO sys_dict_data VALUES (17, 2, '关闭', '1', 'sys_notice_status', '', 'danger', 'N', '0', 'admin', now(), '', NULL, '关闭状态');
INSERT INTO sys_dict_data VALUES (18, 1, '新增', '1', 'sys_oper_type', '', 'info', 'N', '0', 'admin', now(), '', NULL, '新增操作');
INSERT INTO sys_dict_data VALUES (19, 2, '修改', '2', 'sys_oper_type', '', 'info', 'N', '0', 'admin', now(), '', NULL, '修改操作');
INSERT INTO sys_dict_data VALUES (20, 3, '删除', '3', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', now(), '', NULL, '删除操作');
INSERT INTO sys_dict_data VALUES (21, 4, '授权', '4', 'sys_oper_type', '', 'primary', 'N', '0', 'admin', now(), '', NULL, '授权操作');
INSERT INTO sys_dict_data VALUES (22, 5, '导出', '5', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', now(), '', NULL, '导出操作');
INSERT INTO sys_dict_data VALUES (23, 6, '导入', '6', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', now(), '', NULL, '导入操作');
INSERT INTO sys_dict_data VALUES (24, 7, '强退', '7', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', now(), '', NULL, '强退操作');
INSERT INTO sys_dict_data VALUES (25, 8, '生成代码', '8', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', now(), '', NULL, '生成操作');
INSERT INTO sys_dict_data VALUES (26, 9, '清空数据', '9', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', now(), '', NULL, '清空操作');
INSERT INTO sys_dict_data VALUES (27, 1, '成功', '0', 'sys_common_status', '', 'primary', 'N', '0', 'admin', now(), '', NULL, '正常状态');
INSERT INTO sys_dict_data VALUES (28, 2, '失败', '1', 'sys_common_status', '', 'danger', 'N', '0', 'admin', now(), '', NULL, '停用状态');

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS sys_dict_type;
CREATE TABLE sys_dict_type (
  dict_id BIGSERIAL NOT NULL,
  dict_name varchar(100) DEFAULT '',
  dict_type varchar(100) DEFAULT '' unique,
  status char(1) DEFAULT '0',
  create_by varchar(64) ,
  create_time timestamp(6),
  update_by varchar(64) ,
  update_time timestamp(6),
  remark varchar(500) ,
  CONSTRAINT sys_dict_type_pkey PRIMARY KEY (dict_id)
);
COMMENT ON COLUMN sys_dict_type.dict_id IS '字典主键';
COMMENT ON COLUMN sys_dict_type.dict_name IS '字典名称';
COMMENT ON COLUMN sys_dict_type.dict_type IS '字典类型';
COMMENT ON COLUMN sys_dict_type.status IS '状态（0正常 1停用）';
COMMENT ON COLUMN sys_dict_type.create_by IS '创建者';
COMMENT ON COLUMN sys_dict_type.create_time IS '创建时间';
COMMENT ON COLUMN sys_dict_type.update_by IS '更新者';
COMMENT ON COLUMN sys_dict_type.update_time IS '更新时间';
COMMENT ON COLUMN sys_dict_type.remark IS '备注';
COMMENT ON TABLE sys_dict_type IS '字典类型表';

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO sys_dict_type VALUES (1, '用户性别', 'sys_user_sex', '0', 'admin', now(), '', NULL, '用户性别列表');
INSERT INTO sys_dict_type VALUES (2, '菜单状态', 'sys_show_hide', '0', 'admin', now(), '', NULL, '菜单状态列表');
INSERT INTO sys_dict_type VALUES (3, '系统开关', 'sys_normal_disable', '0', 'admin', now(), '', NULL, '系统开关列表');
INSERT INTO sys_dict_type VALUES (4, '任务状态', 'sys_job_status', '0', 'admin', now(), '', NULL, '任务状态列表');
INSERT INTO sys_dict_type VALUES (5, '任务分组', 'sys_job_group', '0', 'admin', now(), '', NULL, '任务分组列表');
INSERT INTO sys_dict_type VALUES (6, '系统是否', 'sys_yes_no', '0', 'admin', now(), '', NULL, '系统是否列表');
INSERT INTO sys_dict_type VALUES (7, '通知类型', 'sys_notice_type', '0', 'admin', now(), '', NULL, '通知类型列表');
INSERT INTO sys_dict_type VALUES (8, '通知状态', 'sys_notice_status', '0', 'admin', now(), '', NULL, '通知状态列表');
INSERT INTO sys_dict_type VALUES (9, '操作类型', 'sys_oper_type', '0', 'admin', now(), '', NULL, '操作类型列表');
INSERT INTO sys_dict_type VALUES (10, '系统状态', 'sys_common_status', '0', 'admin', now(), '', NULL, '登录状态列表');

-- ----------------------------
-- Table structure for sys_job
-- ----------------------------
DROP TABLE IF EXISTS sys_job;
CREATE TABLE sys_job (
  job_id BIGSERIAL NOT NULL,
  job_name varchar(64)  NOT NULL default '',
  job_group varchar(64)  NOT NULL default '',
  invoke_target varchar(500)  NOT NULL,
  cron_expression varchar(255) default '',
  misfire_policy varchar(20) default '3',
  concurrent char(1) default '1',
  status char(1) default '0',
  create_by varchar(64) default '',
  create_time timestamp(6),
  update_by varchar(64) default '',
  update_time timestamp(6),
  remark varchar(500) ,
  CONSTRAINT sys_job_pkey PRIMARY KEY (job_id, job_name, job_group)
);
COMMENT ON COLUMN sys_job.job_id IS '任务ID';
COMMENT ON COLUMN sys_job.job_name IS '任务名称';
COMMENT ON COLUMN sys_job.job_group IS '任务组名';
COMMENT ON COLUMN sys_job.invoke_target IS '调用目标字符串';
COMMENT ON COLUMN sys_job.cron_expression IS 'cron执行表达式';
COMMENT ON COLUMN sys_job.misfire_policy IS '计划执行错误策略（1立即执行 2执行一次 3放弃执行）';
COMMENT ON COLUMN sys_job.concurrent IS '是否并发执行（0允许 1禁止）';
COMMENT ON COLUMN sys_job.status IS '状态（0正常 1暂停）';
COMMENT ON COLUMN sys_job.create_by IS '创建者';
COMMENT ON COLUMN sys_job.create_time IS '创建时间';
COMMENT ON COLUMN sys_job.update_by IS '更新者';
COMMENT ON COLUMN sys_job.update_time IS '更新时间';
COMMENT ON COLUMN sys_job.remark IS '备注信息';
COMMENT ON TABLE sys_job IS '定时任务调度表';

-- ----------------------------
-- Records of sys_job
-- ----------------------------
INSERT INTO sys_job VALUES (1, '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', '0/10 * * * * ?', '3', '1', '1', 'admin', now(), '', NULL, '');
INSERT INTO sys_job VALUES (2, '系统默认（有参）', 'DEFAULT', 'ryTask.ryParams(''ry'')', '0/15 * * * * ?', '3', '1', '1', 'admin', now(), '', NULL, '');
INSERT INTO sys_job VALUES (3, '系统默认（多参）', 'DEFAULT', 'ryTask.ryMultipleParams(''ry'', true, 2000L, 316.50D, 100)', '0/20 * * * * ?', '3', '1', '1', 'admin', now(), '', NULL, '');

-- ----------------------------
-- Table structure for sys_job_log
-- ----------------------------
DROP TABLE IF EXISTS sys_job_log;
CREATE TABLE sys_job_log (
  job_log_id BIGSERIAL NOT NULL,
  job_name varchar(64)  NOT NULL,
  job_group varchar(64)  NOT NULL,
  invoke_target varchar(500)  NOT NULL,
  job_message varchar(500) ,
  status char(1) default '0',
  exception_info varchar(2000) ,
  create_time timestamp(6) ,
  CONSTRAINT sys_job_log_pkey PRIMARY KEY (job_log_id)
);
COMMENT ON COLUMN sys_job_log.job_log_id IS '任务日志ID';
COMMENT ON COLUMN sys_job_log.job_name IS '任务名称';
COMMENT ON COLUMN sys_job_log.job_group IS '任务组名';
COMMENT ON COLUMN sys_job_log.invoke_target IS '调用目标字符串';
COMMENT ON COLUMN sys_job_log.job_message IS '日志信息';
COMMENT ON COLUMN sys_job_log.status IS '执行状态（0正常 1失败）';
COMMENT ON COLUMN sys_job_log.exception_info IS '异常信息';
COMMENT ON COLUMN sys_job_log.create_time IS '创建时间';
COMMENT ON TABLE sys_job_log IS '定时任务调度日志表';

-- ----------------------------
-- Table structure for sys_logininfor
-- ----------------------------
DROP TABLE IF EXISTS sys_logininfor;
CREATE TABLE sys_logininfor (
  info_id BIGSERIAL NOT NULL,
  user_name varchar(50) default '',
  ipaddr varchar(128) default '',
  login_location varchar(255) ,
  browser varchar(50) default '',
  os varchar(50) default '',
  status char(1) default '0',
  msg varchar(255) default '',
  login_time timestamp(6) ,
  CONSTRAINT sys_logininfor_pkey PRIMARY KEY (info_id)
);
COMMENT ON COLUMN sys_logininfor.info_id IS '访问ID';
COMMENT ON COLUMN sys_logininfor.user_name IS '用户账号';
COMMENT ON COLUMN sys_logininfor.ipaddr IS '登录IP地址';
COMMENT ON COLUMN sys_logininfor.login_location IS '登录地点';
COMMENT ON COLUMN sys_logininfor.browser IS '浏览器类型';
COMMENT ON COLUMN sys_logininfor.os IS '操作系统';
COMMENT ON COLUMN sys_logininfor.status IS '登录状态（0成功 1失败）';
COMMENT ON COLUMN sys_logininfor.msg IS '提示消息';
COMMENT ON COLUMN sys_logininfor.login_time IS '访问时间';
COMMENT ON TABLE sys_logininfor IS '系统访问记录';

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS sys_menu;
CREATE TABLE sys_menu (
  menu_id BIGSERIAL NOT NULL,
  menu_name varchar(50)  NOT NULL,
  parent_id int8 default 0,
  order_num int4 default 0,
  path varchar(200) default '',
  component varchar(255) default '',
  is_frame int4 default 1,
  is_cache int4 default 0,
  menu_type char(1) default '',
  visible char(1) default 0,
  status char(1) default 0,
  perms varchar(100) default '',
  icon varchar(100) default '',
  create_by varchar(64) default '',
  create_time timestamp(6),
  update_by varchar(64) default '',
  update_time timestamp(6),
  remark varchar(500) ,
  CONSTRAINT sys_menu_pkey PRIMARY KEY (menu_id)
);
COMMENT ON COLUMN sys_menu.menu_id IS '菜单ID';
COMMENT ON COLUMN sys_menu.menu_name IS '菜单名称';
COMMENT ON COLUMN sys_menu.parent_id IS '父菜单ID';
COMMENT ON COLUMN sys_menu.order_num IS '显示顺序';
COMMENT ON COLUMN sys_menu.path IS '路由地址';
COMMENT ON COLUMN sys_menu.component IS '组件路径';
COMMENT ON COLUMN sys_menu.is_frame IS '是否为外链（0是 1否）';
COMMENT ON COLUMN sys_menu.is_cache IS '是否缓存（0缓存 1不缓存）';
COMMENT ON COLUMN sys_menu.menu_type IS '菜单类型（M目录 C菜单 F按钮）';
COMMENT ON COLUMN sys_menu.visible IS '菜单状态（0显示 1隐藏）';
COMMENT ON COLUMN sys_menu.status IS '菜单状态（0正常 1停用）';
COMMENT ON COLUMN sys_menu.perms IS '权限标识';
COMMENT ON COLUMN sys_menu.icon IS '菜单图标';
COMMENT ON COLUMN sys_menu.create_by IS '创建者';
COMMENT ON COLUMN sys_menu.create_time IS '创建时间';
COMMENT ON COLUMN sys_menu.update_by IS '更新者';
COMMENT ON COLUMN sys_menu.update_time IS '更新时间';
COMMENT ON COLUMN sys_menu.remark IS '备注';
COMMENT ON TABLE sys_menu IS '菜单权限表';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO sys_menu VALUES (1, '系统管理', 0, 1, 'system', NULL, 1, 0, 'M', '0', '0', '', 'system', 'admin', now(), '', NULL, '系统管理目录');
INSERT INTO sys_menu VALUES (2, '系统监控', 0, 2, 'monitor', NULL, 1, 0, 'M', '0', '0', '', 'monitor', 'admin', now(), '', NULL, '系统监控目录');
INSERT INTO sys_menu VALUES (3, '系统工具', 0, 3, 'tool', NULL, 1, 0, 'M', '0', '0', '', 'tool', 'admin', now(), '', NULL, '系统工具目录');
INSERT INTO sys_menu VALUES (4, '若依官网', 0, 4, 'http://ruoyi.vip', NULL, 0, 0, 'M', '0', '0', '', 'guide', 'admin', now(), '', NULL, '若依官网地址');
INSERT INTO sys_menu VALUES (100, '用户管理', 1, 1, 'user', 'system/user/index', 1, 0, 'C', '0', '0', 'system:user:list', 'user', 'admin', now(), '', NULL, '用户管理菜单');
INSERT INTO sys_menu VALUES (101, '角色管理', 1, 2, 'role', 'system/role/index', 1, 0, 'C', '0', '0', 'system:role:list', 'peoples', 'admin', now(), '', NULL, '角色管理菜单');
INSERT INTO sys_menu VALUES (102, '菜单管理', 1, 3, 'menu', 'system/menu/index', 1, 0, 'C', '0', '0', 'system:menu:list', 'tree-table', 'admin', now(), '', NULL, '菜单管理菜单');
INSERT INTO sys_menu VALUES (103, '部门管理', 1, 4, 'dept', 'system/dept/index', 1, 0, 'C', '0', '0', 'system:dept:list', 'tree', 'admin', now(), '', NULL, '部门管理菜单');
INSERT INTO sys_menu VALUES (104, '岗位管理', 1, 5, 'post', 'system/post/index', 1, 0, 'C', '0', '0', 'system:post:list', 'post', 'admin', now(), '', NULL, '岗位管理菜单');
INSERT INTO sys_menu VALUES (105, '字典管理', 1, 6, 'dict', 'system/dict/index', 1, 0, 'C', '0', '0', 'system:dict:list', 'dict', 'admin', now(), '', NULL, '字典管理菜单');
INSERT INTO sys_menu VALUES (106, '参数设置', 1, 7, 'config', 'system/config/index', 1, 0, 'C', '0', '0', 'system:config:list', 'edit', 'admin', now(), '', NULL, '参数设置菜单');
INSERT INTO sys_menu VALUES (107, '通知公告', 1, 8, 'notice', 'system/notice/index', 1, 0, 'C', '0', '0', 'system:notice:list', 'message', 'admin', now(), '', NULL, '通知公告菜单');
INSERT INTO sys_menu VALUES (108, '日志管理', 1, 9, 'log', '', 1, 0, 'M', '0', '0', '', 'log', 'admin', now(), '', NULL, '日志管理菜单');
INSERT INTO sys_menu VALUES (109, '在线用户', 2, 1, 'online', 'monitor/online/index', 1, 0, 'C', '0', '0', 'monitor:online:list', 'online', 'admin', now(), '', NULL, '在线用户菜单');
INSERT INTO sys_menu VALUES (110, '定时任务', 2, 2, 'job', 'monitor/job/index', 1, 0, 'C', '0', '0', 'monitor:job:list', 'job', 'admin', now(), '', NULL, '定时任务菜单');
INSERT INTO sys_menu VALUES (111, '数据监控', 2, 3, 'druid', 'monitor/druid/index', 1, 0, 'C', '0', '0', 'monitor:druid:list', 'druid', 'admin', now(), '', NULL, '数据监控菜单');
INSERT INTO sys_menu VALUES (112, '服务监控', 2, 4, 'server', 'monitor/server/index', 1, 0, 'C', '0', '0', 'monitor:server:list', 'server', 'admin', now(), '', NULL, '服务监控菜单');
INSERT INTO sys_menu VALUES (113, '缓存监控', 2, 5, 'cache', 'monitor/cache/index', 1, 0, 'C', '0', '0', 'monitor:cache:list', 'redis', 'admin', now(), '', NULL, '缓存监控菜单');
INSERT INTO sys_menu VALUES (114, '表单构建', 3, 1, 'build', 'tool/build/index', 1, 0, 'C', '0', '0', 'tool:build:list', 'build', 'admin', now(), '', NULL, '表单构建菜单');
INSERT INTO sys_menu VALUES (115, '代码生成', 3, 2, 'gen', 'tool/gen/index', 1, 0, 'C', '0', '0', 'tool:gen:list', 'code', 'admin', now(), '', NULL, '代码生成菜单');
INSERT INTO sys_menu VALUES (116, '系统接口', 3, 3, 'swagger', 'tool/swagger/index', 1, 0, 'C', '0', '0', 'tool:swagger:list', 'swagger', 'admin', now(), '', NULL, '系统接口菜单');
INSERT INTO sys_menu VALUES (500, '操作日志', 108, 1, 'operlog', 'monitor/operlog/index', 1, 0, 'C', '0', '0', 'monitor:operlog:list', 'form', 'admin', now(), '', NULL, '操作日志菜单');
INSERT INTO sys_menu VALUES (501, '登录日志', 108, 2, 'logininfor', 'monitor/logininfor/index', 1, 0, 'C', '0', '0', 'monitor:logininfor:list', 'logininfor', 'admin', now(), '', NULL, '登录日志菜单');
INSERT INTO sys_menu VALUES (1001, '用户查询', 100, 1, '', '', 1, 0, 'F', '0', '0', 'system:user:query', '#', 'admin', now(), '', NULL, '');
INSERT INTO sys_menu VALUES (1002, '用户新增', 100, 2, '', '', 1, 0, 'F', '0', '0', 'system:user:add', '#', 'admin', now(), '', NULL, '');
INSERT INTO sys_menu VALUES (1003, '用户修改', 100, 3, '', '', 1, 0, 'F', '0', '0', 'system:user:edit', '#', 'admin', now(), '', NULL, '');
INSERT INTO sys_menu VALUES (1004, '用户删除', 100, 4, '', '', 1, 0, 'F', '0', '0', 'system:user:remove', '#', 'admin', now(), '', NULL, '');
INSERT INTO sys_menu VALUES (1005, '用户导出', 100, 5, '', '', 1, 0, 'F', '0', '0', 'system:user:export', '#', 'admin', now(), '', NULL, '');
INSERT INTO sys_menu VALUES (1006, '用户导入', 100, 6, '', '', 1, 0, 'F', '0', '0', 'system:user:import', '#', 'admin', now(), '', NULL, '');
INSERT INTO sys_menu VALUES (1007, '重置密码', 100, 7, '', '', 1, 0, 'F', '0', '0', 'system:user:resetPwd', '#', 'admin', now(), '', NULL, '');
INSERT INTO sys_menu VALUES (1008, '角色查询', 101, 1, '', '', 1, 0, 'F', '0', '0', 'system:role:query', '#', 'admin', now(), '', NULL, '');
INSERT INTO sys_menu VALUES (1009, '角色新增', 101, 2, '', '', 1, 0, 'F', '0', '0', 'system:role:add', '#', 'admin', now(), '', NULL, '');
INSERT INTO sys_menu VALUES (1010, '角色修改', 101, 3, '', '', 1, 0, 'F', '0', '0', 'system:role:edit', '#', 'admin', now(), '', NULL, '');
INSERT INTO sys_menu VALUES (1011, '角色删除', 101, 4, '', '', 1, 0, 'F', '0', '0', 'system:role:remove', '#', 'admin', now(), '', NULL, '');
INSERT INTO sys_menu VALUES (1012, '角色导出', 101, 5, '', '', 1, 0, 'F', '0', '0', 'system:role:export', '#', 'admin', now(), '', NULL, '');
INSERT INTO sys_menu VALUES (1013, '菜单查询', 102, 1, '', '', 1, 0, 'F', '0', '0', 'system:menu:query', '#', 'admin', now(), '', NULL, '');
INSERT INTO sys_menu VALUES (1014, '菜单新增', 102, 2, '', '', 1, 0, 'F', '0', '0', 'system:menu:add', '#', 'admin', now(), '', NULL, '');
INSERT INTO sys_menu VALUES (1015, '菜单修改', 102, 3, '', '', 1, 0, 'F', '0', '0', 'system:menu:edit', '#', 'admin', now(), '', NULL, '');
INSERT INTO sys_menu VALUES (1016, '菜单删除', 102, 4, '', '', 1, 0, 'F', '0', '0', 'system:menu:remove', '#', 'admin', now(), '', NULL, '');
INSERT INTO sys_menu VALUES (1017, '部门查询', 103, 1, '', '', 1, 0, 'F', '0', '0', 'system:dept:query', '#', 'admin', now(), '', NULL, '');
INSERT INTO sys_menu VALUES (1018, '部门新增', 103, 2, '', '', 1, 0, 'F', '0', '0', 'system:dept:add', '#', 'admin', now(), '', NULL, '');
INSERT INTO sys_menu VALUES (1019, '部门修改', 103, 3, '', '', 1, 0, 'F', '0', '0', 'system:dept:edit', '#', 'admin', now(), '', NULL, '');
INSERT INTO sys_menu VALUES (1020, '部门删除', 103, 4, '', '', 1, 0, 'F', '0', '0', 'system:dept:remove', '#', 'admin', now(), '', NULL, '');
INSERT INTO sys_menu VALUES (1021, '岗位查询', 104, 1, '', '', 1, 0, 'F', '0', '0', 'system:post:query', '#', 'admin', now(), '', NULL, '');
INSERT INTO sys_menu VALUES (1022, '岗位新增', 104, 2, '', '', 1, 0, 'F', '0', '0', 'system:post:add', '#', 'admin', now(), '', NULL, '');
INSERT INTO sys_menu VALUES (1023, '岗位修改', 104, 3, '', '', 1, 0, 'F', '0', '0', 'system:post:edit', '#', 'admin', now(), '', NULL, '');
INSERT INTO sys_menu VALUES (1024, '岗位删除', 104, 4, '', '', 1, 0, 'F', '0', '0', 'system:post:remove', '#', 'admin', now(), '', NULL, '');
INSERT INTO sys_menu VALUES (1025, '岗位导出', 104, 5, '', '', 1, 0, 'F', '0', '0', 'system:post:export', '#', 'admin', now(), '', NULL, '');
INSERT INTO sys_menu VALUES (1026, '字典查询', 105, 1, '#', '', 1, 0, 'F', '0', '0', 'system:dict:query', '#', 'admin', now(), '', NULL, '');
INSERT INTO sys_menu VALUES (1027, '字典新增', 105, 2, '#', '', 1, 0, 'F', '0', '0', 'system:dict:add', '#', 'admin', now(), '', NULL, '');
INSERT INTO sys_menu VALUES (1028, '字典修改', 105, 3, '#', '', 1, 0, 'F', '0', '0', 'system:dict:edit', '#', 'admin', now(), '', NULL, '');
INSERT INTO sys_menu VALUES (1029, '字典删除', 105, 4, '#', '', 1, 0, 'F', '0', '0', 'system:dict:remove', '#', 'admin', now(), '', NULL, '');
INSERT INTO sys_menu VALUES (1030, '字典导出', 105, 5, '#', '', 1, 0, 'F', '0', '0', 'system:dict:export', '#', 'admin', now(), '', NULL, '');
INSERT INTO sys_menu VALUES (1031, '参数查询', 106, 1, '#', '', 1, 0, 'F', '0', '0', 'system:config:query', '#', 'admin', now(), '', NULL, '');
INSERT INTO sys_menu VALUES (1032, '参数新增', 106, 2, '#', '', 1, 0, 'F', '0', '0', 'system:config:add', '#', 'admin', now(), '', NULL, '');
INSERT INTO sys_menu VALUES (1033, '参数修改', 106, 3, '#', '', 1, 0, 'F', '0', '0', 'system:config:edit', '#', 'admin', now(), '', NULL, '');
INSERT INTO sys_menu VALUES (1034, '参数删除', 106, 4, '#', '', 1, 0, 'F', '0', '0', 'system:config:remove', '#', 'admin', now(), '', NULL, '');
INSERT INTO sys_menu VALUES (1035, '参数导出', 106, 5, '#', '', 1, 0, 'F', '0', '0', 'system:config:export', '#', 'admin', now(), '', NULL, '');
INSERT INTO sys_menu VALUES (1036, '公告查询', 107, 1, '#', '', 1, 0, 'F', '0', '0', 'system:notice:query', '#', 'admin', now(), '', NULL, '');
INSERT INTO sys_menu VALUES (1037, '公告新增', 107, 2, '#', '', 1, 0, 'F', '0', '0', 'system:notice:add', '#', 'admin', now(), '', NULL, '');
INSERT INTO sys_menu VALUES (1038, '公告修改', 107, 3, '#', '', 1, 0, 'F', '0', '0', 'system:notice:edit', '#', 'admin', now(), '', NULL, '');
INSERT INTO sys_menu VALUES (1039, '公告删除', 107, 4, '#', '', 1, 0, 'F', '0', '0', 'system:notice:remove', '#', 'admin', now(), '', NULL, '');
INSERT INTO sys_menu VALUES (1040, '操作查询', 500, 1, '#', '', 1, 0, 'F', '0', '0', 'monitor:operlog:query', '#', 'admin', now(), '', NULL, '');
INSERT INTO sys_menu VALUES (1041, '操作删除', 500, 2, '#', '', 1, 0, 'F', '0', '0', 'monitor:operlog:remove', '#', 'admin', now(), '', NULL, '');
INSERT INTO sys_menu VALUES (1042, '日志导出', 500, 4, '#', '', 1, 0, 'F', '0', '0', 'monitor:operlog:export', '#', 'admin', now(), '', NULL, '');
INSERT INTO sys_menu VALUES (1043, '登录查询', 501, 1, '#', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:query', '#', 'admin', now(), '', NULL, '');
INSERT INTO sys_menu VALUES (1044, '登录删除', 501, 2, '#', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:remove', '#', 'admin', now(), '', NULL, '');
INSERT INTO sys_menu VALUES (1045, '日志导出', 501, 3, '#', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:export', '#', 'admin', now(), '', NULL, '');
INSERT INTO sys_menu VALUES (1046, '在线查询', 109, 1, '#', '', 1, 0, 'F', '0', '0', 'monitor:online:query', '#', 'admin', now(), '', NULL, '');
INSERT INTO sys_menu VALUES (1047, '批量强退', 109, 2, '#', '', 1, 0, 'F', '0', '0', 'monitor:online:batchLogout', '#', 'admin', now(), '', NULL, '');
INSERT INTO sys_menu VALUES (1048, '单条强退', 109, 3, '#', '', 1, 0, 'F', '0', '0', 'monitor:online:forceLogout', '#', 'admin', now(), '', NULL, '');
INSERT INTO sys_menu VALUES (1049, '任务查询', 110, 1, '#', '', 1, 0, 'F', '0', '0', 'monitor:job:query', '#', 'admin', now(), '', NULL, '');
INSERT INTO sys_menu VALUES (1050, '任务新增', 110, 2, '#', '', 1, 0, 'F', '0', '0', 'monitor:job:add', '#', 'admin', now(), '', NULL, '');
INSERT INTO sys_menu VALUES (1051, '任务修改', 110, 3, '#', '', 1, 0, 'F', '0', '0', 'monitor:job:edit', '#', 'admin', now(), '', NULL, '');
INSERT INTO sys_menu VALUES (1052, '任务删除', 110, 4, '#', '', 1, 0, 'F', '0', '0', 'monitor:job:remove', '#', 'admin', now(), '', NULL, '');
INSERT INTO sys_menu VALUES (1053, '状态修改', 110, 5, '#', '', 1, 0, 'F', '0', '0', 'monitor:job:changeStatus', '#', 'admin', now(), '', NULL, '');
INSERT INTO sys_menu VALUES (1054, '任务导出', 110, 7, '#', '', 1, 0, 'F', '0', '0', 'monitor:job:export', '#', 'admin', now(), '', NULL, '');
INSERT INTO sys_menu VALUES (1055, '生成查询', 115, 1, '#', '', 1, 0, 'F', '0', '0', 'tool:gen:query', '#', 'admin', now(), '', NULL, '');
INSERT INTO sys_menu VALUES (1056, '生成修改', 115, 2, '#', '', 1, 0, 'F', '0', '0', 'tool:gen:edit', '#', 'admin', now(), '', NULL, '');
INSERT INTO sys_menu VALUES (1057, '生成删除', 115, 3, '#', '', 1, 0, 'F', '0', '0', 'tool:gen:remove', '#', 'admin', now(), '', NULL, '');
INSERT INTO sys_menu VALUES (1058, '导入代码', 115, 2, '#', '', 1, 0, 'F', '0', '0', 'tool:gen:import', '#', 'admin', now(), '', NULL, '');
INSERT INTO sys_menu VALUES (1059, '预览代码', 115, 4, '#', '', 1, 0, 'F', '0', '0', 'tool:gen:preview', '#', 'admin', now(), '', NULL, '');
INSERT INTO sys_menu VALUES (1060, '生成代码', 115, 5, '#', '', 1, 0, 'F', '0', '0', 'tool:gen:code', '#', 'admin', now(), '', NULL, '');

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS sys_notice;
CREATE TABLE sys_notice (
  notice_id BIGSERIAL NOT NULL,
  notice_title varchar(50)  NOT NULL,
  notice_type char(1)  NOT NULL,
  notice_content text,
  status char(1) default '0',
  create_by varchar(64) default '',
  create_time timestamp(6),
  update_by varchar(64) default '',
  update_time timestamp(6),
  remark varchar(255) ,
  CONSTRAINT sys_notice_pkey PRIMARY KEY (notice_id)
);
COMMENT ON COLUMN sys_notice.notice_id IS '公告ID';
COMMENT ON COLUMN sys_notice.notice_title IS '公告标题';
COMMENT ON COLUMN sys_notice.notice_type IS '公告类型（1通知 2公告）';
COMMENT ON COLUMN sys_notice.notice_content IS '公告内容';
COMMENT ON COLUMN sys_notice.status IS '公告状态（0正常 1关闭）';
COMMENT ON COLUMN sys_notice.create_by IS '创建者';
COMMENT ON COLUMN sys_notice.create_time IS '创建时间';
COMMENT ON COLUMN sys_notice.update_by IS '更新者';
COMMENT ON COLUMN sys_notice.update_time IS '更新时间';
COMMENT ON COLUMN sys_notice.remark IS '备注';
COMMENT ON TABLE sys_notice IS '通知公告表';

-- ----------------------------
-- Table structure for sys_oper_log
-- ----------------------------
DROP TABLE IF EXISTS sys_oper_log;
CREATE TABLE sys_oper_log (
  oper_id BIGSERIAL NOT NULL,
  title varchar(50) default '',
  business_type int4 default 0,
  method varchar(100) default '',
  request_method varchar(10) default '',
  operator_type int4 default 0,
  oper_name varchar(50) default '',
  dept_name varchar(50) default '',
  oper_url varchar(255) default '',
  oper_ip varchar(128) default '',
  oper_location varchar(255) default '',
  oper_param varchar(2000) default '',
  json_result varchar(2000) default '',
  status CHAR(1) default '0',
  error_msg varchar(2000) ,
  oper_time timestamp(6) ,
  CONSTRAINT sys_oper_log_pkey PRIMARY KEY (oper_id)
);
COMMENT ON COLUMN sys_oper_log.oper_id IS '日志主键';
COMMENT ON COLUMN sys_oper_log.title IS '模块标题';
COMMENT ON COLUMN sys_oper_log.business_type IS '业务类型（0其它 1新增 2修改 3删除）';
COMMENT ON COLUMN sys_oper_log.method IS '方法名称';
COMMENT ON COLUMN sys_oper_log.request_method IS '请求方式';
COMMENT ON COLUMN sys_oper_log.operator_type IS '操作类别（0其它 1后台用户 2手机端用户）';
COMMENT ON COLUMN sys_oper_log.oper_name IS '操作人员';
COMMENT ON COLUMN sys_oper_log.dept_name IS '部门名称';
COMMENT ON COLUMN sys_oper_log.oper_url IS '请求URL';
COMMENT ON COLUMN sys_oper_log.oper_ip IS '主机地址';
COMMENT ON COLUMN sys_oper_log.oper_location IS '操作地点';
COMMENT ON COLUMN sys_oper_log.oper_param IS '请求参数';
COMMENT ON COLUMN sys_oper_log.json_result IS '返回参数';
COMMENT ON COLUMN sys_oper_log.status IS '操作状态（0正常 1异常）';
COMMENT ON COLUMN sys_oper_log.error_msg IS '错误消息';
COMMENT ON COLUMN sys_oper_log.oper_time IS '操作时间';
COMMENT ON TABLE sys_oper_log IS '操作日志记录';

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS sys_post;
CREATE TABLE sys_post (
  post_id BIGSERIAL NOT NULL,
  post_code varchar(64)  NOT NULL,
  post_name varchar(50)  NOT NULL,
  post_sort int4 NOT NULL,
  status char(1)  default '0',
  create_by varchar(64) default '',
  create_time timestamp(6),
  update_by varchar(64) default '',
  update_time timestamp(6),
  remark varchar(500) ,
  CONSTRAINT sys_post_pkey PRIMARY KEY (post_id)
);
COMMENT ON COLUMN sys_post.post_id IS '岗位ID';
COMMENT ON COLUMN sys_post.post_code IS '岗位编码';
COMMENT ON COLUMN sys_post.post_name IS '岗位名称';
COMMENT ON COLUMN sys_post.post_sort IS '显示顺序';
COMMENT ON COLUMN sys_post.status IS '状态（0正常 1停用）';
COMMENT ON COLUMN sys_post.create_by IS '创建者';
COMMENT ON COLUMN sys_post.create_time IS '创建时间';
COMMENT ON COLUMN sys_post.update_by IS '更新者';
COMMENT ON COLUMN sys_post.update_time IS '更新时间';
COMMENT ON COLUMN sys_post.remark IS '备注';
COMMENT ON TABLE sys_post IS '岗位信息表';

-- ----------------------------
-- Records of sys_post
-- ----------------------------
INSERT INTO sys_post VALUES (1, 'ceo', '董事长', 1, '0', 'admin', now(), '', NULL, '');
INSERT INTO sys_post VALUES (2, 'se', '项目经理', 2, '0', 'admin', now(), '', NULL, '');
INSERT INTO sys_post VALUES (3, 'hr', '人力资源', 3, '0', 'admin', now(), '', NULL, '');
INSERT INTO sys_post VALUES (4, 'user', '普通员工', 4, '0', 'admin', now(), '', NULL, '');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS sys_role;
CREATE TABLE sys_role (
  role_id BIGSERIAL NOT NULL,
  role_name varchar(30)  NOT NULL,
  role_key varchar(100)  NOT NULL,
  role_sort int4 NOT NULL,
  data_scope char(1) default '1',
  menu_check_strictly bool default '1',
  dept_check_strictly bool default '1',
  status char(1)  default '0',
  del_flag char(1) default '0',
  create_by varchar(64) default '',
  create_time timestamp(6),
  update_by varchar(64) default '',
  update_time timestamp(6),
  remark varchar(500) ,
  CONSTRAINT sys_role_pkey PRIMARY KEY (role_id)
);
COMMENT ON COLUMN sys_role.role_id IS '角色ID';
COMMENT ON COLUMN sys_role.role_name IS '角色名称';
COMMENT ON COLUMN sys_role.role_key IS '角色权限字符串';
COMMENT ON COLUMN sys_role.role_sort IS '显示顺序';
COMMENT ON COLUMN sys_role.data_scope IS '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）';
COMMENT ON COLUMN sys_role.menu_check_strictly IS '菜单树选择项是否关联显示';
COMMENT ON COLUMN sys_role.dept_check_strictly IS '部门树选择项是否关联显示';
COMMENT ON COLUMN sys_role.status IS '角色状态（0正常 1停用）';
COMMENT ON COLUMN sys_role.del_flag IS '删除标志（0代表存在 2代表删除）';
COMMENT ON COLUMN sys_role.create_by IS '创建者';
COMMENT ON COLUMN sys_role.create_time IS '创建时间';
COMMENT ON COLUMN sys_role.update_by IS '更新者';
COMMENT ON COLUMN sys_role.update_time IS '更新时间';
COMMENT ON COLUMN sys_role.remark IS '备注';
COMMENT ON TABLE sys_role IS '角色信息表';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO sys_role VALUES (1, '超级管理员', 'admin', 1, '1', true, true, '0', '0', 'admin', now(), '', NULL, '超级管理员');
INSERT INTO sys_role VALUES (2, '普通角色', 'common', 2, '2', true, true, '0', '0', 'admin', now(), '', NULL, '普通角色');

-- ----------------------------
-- Table structure for sys_role_dept
-- ----------------------------
DROP TABLE IF EXISTS sys_role_dept;
CREATE TABLE sys_role_dept (
  role_id int8 NOT NULL,
  dept_id int8 NOT NULL,
  CONSTRAINT sys_role_dept_pkey PRIMARY KEY (role_id, dept_id)
);
COMMENT ON COLUMN sys_role_dept.role_id IS '角色ID';
COMMENT ON COLUMN sys_role_dept.dept_id IS '部门ID';
COMMENT ON TABLE sys_role_dept IS '角色和部门关联表';

-- ----------------------------
-- Records of sys_role_dept
-- ----------------------------
INSERT INTO sys_role_dept VALUES (2, 100);
INSERT INTO sys_role_dept VALUES (2, 101);
INSERT INTO sys_role_dept VALUES (2, 105);

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS sys_role_menu;
CREATE TABLE sys_role_menu (
  role_id int8 NOT NULL,
  menu_id int8 NOT NULL,
  CONSTRAINT sys_role_menu_pkey PRIMARY KEY (role_id, menu_id)
);
COMMENT ON COLUMN sys_role_menu.role_id IS '角色ID';
COMMENT ON COLUMN sys_role_menu.menu_id IS '菜单ID';
COMMENT ON TABLE sys_role_menu IS '角色和菜单关联表';

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO sys_role_menu VALUES (2, 1);
INSERT INTO sys_role_menu VALUES (2, 2);
INSERT INTO sys_role_menu VALUES (2, 3);
INSERT INTO sys_role_menu VALUES (2, 4);
INSERT INTO sys_role_menu VALUES (2, 100);
INSERT INTO sys_role_menu VALUES (2, 101);
INSERT INTO sys_role_menu VALUES (2, 102);
INSERT INTO sys_role_menu VALUES (2, 103);
INSERT INTO sys_role_menu VALUES (2, 104);
INSERT INTO sys_role_menu VALUES (2, 105);
INSERT INTO sys_role_menu VALUES (2, 106);
INSERT INTO sys_role_menu VALUES (2, 107);
INSERT INTO sys_role_menu VALUES (2, 108);
INSERT INTO sys_role_menu VALUES (2, 109);
INSERT INTO sys_role_menu VALUES (2, 110);
INSERT INTO sys_role_menu VALUES (2, 111);
INSERT INTO sys_role_menu VALUES (2, 112);
INSERT INTO sys_role_menu VALUES (2, 113);
INSERT INTO sys_role_menu VALUES (2, 114);
INSERT INTO sys_role_menu VALUES (2, 115);
INSERT INTO sys_role_menu VALUES (2, 116);
INSERT INTO sys_role_menu VALUES (2, 500);
INSERT INTO sys_role_menu VALUES (2, 501);
INSERT INTO sys_role_menu VALUES (2, 1000);
INSERT INTO sys_role_menu VALUES (2, 1001);
INSERT INTO sys_role_menu VALUES (2, 1002);
INSERT INTO sys_role_menu VALUES (2, 1003);
INSERT INTO sys_role_menu VALUES (2, 1004);
INSERT INTO sys_role_menu VALUES (2, 1005);
INSERT INTO sys_role_menu VALUES (2, 1006);
INSERT INTO sys_role_menu VALUES (2, 1007);
INSERT INTO sys_role_menu VALUES (2, 1008);
INSERT INTO sys_role_menu VALUES (2, 1009);
INSERT INTO sys_role_menu VALUES (2, 1010);
INSERT INTO sys_role_menu VALUES (2, 1011);
INSERT INTO sys_role_menu VALUES (2, 1012);
INSERT INTO sys_role_menu VALUES (2, 1013);
INSERT INTO sys_role_menu VALUES (2, 1014);
INSERT INTO sys_role_menu VALUES (2, 1015);
INSERT INTO sys_role_menu VALUES (2, 1016);
INSERT INTO sys_role_menu VALUES (2, 1017);
INSERT INTO sys_role_menu VALUES (2, 1018);
INSERT INTO sys_role_menu VALUES (2, 1019);
INSERT INTO sys_role_menu VALUES (2, 1020);
INSERT INTO sys_role_menu VALUES (2, 1021);
INSERT INTO sys_role_menu VALUES (2, 1022);
INSERT INTO sys_role_menu VALUES (2, 1023);
INSERT INTO sys_role_menu VALUES (2, 1024);
INSERT INTO sys_role_menu VALUES (2, 1025);
INSERT INTO sys_role_menu VALUES (2, 1026);
INSERT INTO sys_role_menu VALUES (2, 1027);
INSERT INTO sys_role_menu VALUES (2, 1028);
INSERT INTO sys_role_menu VALUES (2, 1029);
INSERT INTO sys_role_menu VALUES (2, 1030);
INSERT INTO sys_role_menu VALUES (2, 1031);
INSERT INTO sys_role_menu VALUES (2, 1032);
INSERT INTO sys_role_menu VALUES (2, 1033);
INSERT INTO sys_role_menu VALUES (2, 1034);
INSERT INTO sys_role_menu VALUES (2, 1035);
INSERT INTO sys_role_menu VALUES (2, 1036);
INSERT INTO sys_role_menu VALUES (2, 1037);
INSERT INTO sys_role_menu VALUES (2, 1038);
INSERT INTO sys_role_menu VALUES (2, 1039);
INSERT INTO sys_role_menu VALUES (2, 1040);
INSERT INTO sys_role_menu VALUES (2, 1041);
INSERT INTO sys_role_menu VALUES (2, 1042);
INSERT INTO sys_role_menu VALUES (2, 1043);
INSERT INTO sys_role_menu VALUES (2, 1044);
INSERT INTO sys_role_menu VALUES (2, 1045);
INSERT INTO sys_role_menu VALUES (2, 1046);
INSERT INTO sys_role_menu VALUES (2, 1047);
INSERT INTO sys_role_menu VALUES (2, 1048);
INSERT INTO sys_role_menu VALUES (2, 1049);
INSERT INTO sys_role_menu VALUES (2, 1050);
INSERT INTO sys_role_menu VALUES (2, 1051);
INSERT INTO sys_role_menu VALUES (2, 1052);
INSERT INTO sys_role_menu VALUES (2, 1053);
INSERT INTO sys_role_menu VALUES (2, 1054);
INSERT INTO sys_role_menu VALUES (2, 1055);
INSERT INTO sys_role_menu VALUES (2, 1056);
INSERT INTO sys_role_menu VALUES (2, 1057);
INSERT INTO sys_role_menu VALUES (2, 1058);
INSERT INTO sys_role_menu VALUES (2, 1059);
INSERT INTO sys_role_menu VALUES (2, 1060);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS sys_user;
CREATE TABLE sys_user (
  user_id BIGSERIAL NOT NULL,
  dept_id int8 default null,
  user_name varchar(30)  NOT NULL,
  nick_name varchar(30)  NOT NULL,
  user_type varchar(2) default '00',
  email varchar(50) default '',
  phonenumber varchar(11) default '',
  sex char(1) default '0',
  avatar varchar(100) ,
  password varchar(100) default '',
  status char(1) default '0',
  del_flag char(1) default '0',
  login_ip varchar(128) default '',
  login_date timestamp(6),
  create_by varchar(64) default '',
  create_time timestamp(6),
  update_by varchar(64) default '',
  update_time timestamp(6),
  remark varchar(500) ,
  CONSTRAINT sys_user_pkey PRIMARY KEY (user_id)
);
COMMENT ON COLUMN sys_user.user_id IS '用户ID';
COMMENT ON COLUMN sys_user.dept_id IS '部门ID';
COMMENT ON COLUMN sys_user.user_name IS '用户账号';
COMMENT ON COLUMN sys_user.nick_name IS '用户昵称';
COMMENT ON COLUMN sys_user.user_type IS '用户类型（00系统用户）';
COMMENT ON COLUMN sys_user.email IS '用户邮箱';
COMMENT ON COLUMN sys_user.phonenumber IS '手机号码';
COMMENT ON COLUMN sys_user.sex IS '用户性别（0男 1女 2未知）';
COMMENT ON COLUMN sys_user.avatar IS '头像地址';
COMMENT ON COLUMN sys_user.password IS '密码';
COMMENT ON COLUMN sys_user.status IS '帐号状态（0正常 1停用）';
COMMENT ON COLUMN sys_user.del_flag IS '删除标志（0代表存在 2代表删除）';
COMMENT ON COLUMN sys_user.login_ip IS '最后登录IP';
COMMENT ON COLUMN sys_user.login_date IS '最后登录时间';
COMMENT ON COLUMN sys_user.create_by IS '创建者';
COMMENT ON COLUMN sys_user.create_time IS '创建时间';
COMMENT ON COLUMN sys_user.update_by IS '更新者';
COMMENT ON COLUMN sys_user.update_time IS '更新时间';
COMMENT ON COLUMN sys_user.remark IS '备注';
COMMENT ON TABLE sys_user IS '用户信息表';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO sys_user VALUES (2, 105, 'ry', '若依', '00', 'ry@qq.com', '15666666666', '1', '', '$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2', '0', '0', '127.0.0.1', now(), 'admin', now(), '', NULL, '测试员');
INSERT INTO sys_user VALUES (1, 103, 'admin', '若依', '00', 'ry@163.com', '15888888888', '1', '', '$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2', '0', '0', '127.0.0.1', '2021-07-30 15:26:40.156', 'admin', now(), '', '2021-07-30 15:26:40.161813', '管理员');

-- ----------------------------
-- Table structure for sys_user_post
-- ----------------------------
DROP TABLE IF EXISTS sys_user_post;
CREATE TABLE sys_user_post (
  user_id int8 NOT NULL,
  post_id int8 NOT NULL,
  CONSTRAINT sys_user_post_pkey PRIMARY KEY (user_id, post_id)
);
COMMENT ON COLUMN sys_user_post.user_id IS '用户ID';
COMMENT ON COLUMN sys_user_post.post_id IS '岗位ID';
COMMENT ON TABLE sys_user_post IS '用户与岗位关联表';

-- ----------------------------
-- Records of sys_user_post
-- ----------------------------
INSERT INTO sys_user_post VALUES (1, 1);
INSERT INTO sys_user_post VALUES (2, 2);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS sys_user_role;
CREATE TABLE sys_user_role (
  user_id int8 NOT NULL,
  role_id int8 NOT NULL,
  CONSTRAINT sys_user_role_pkey PRIMARY KEY (user_id, role_id)
);
COMMENT ON COLUMN sys_user_role.user_id IS '用户ID';
COMMENT ON COLUMN sys_user_role.role_id IS '角色ID';
COMMENT ON TABLE sys_user_role IS '用户和角色关联表';

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO sys_user_role VALUES (1, 1);
INSERT INTO sys_user_role VALUES (2, 2);

-- ----------------------------
-- Function structure for find_in_set
-- ----------------------------
CREATE OR REPLACE FUNCTION find_in_set(BIGINT, VARCHAR)
    RETURNS BOOLEAN
    AS $BODY$
DECLARE
    STR ALIAS FOR $1;
    STRS ALIAS FOR $2;
    POS    INTEGER;
    STATUS BOOLEAN;
BEGIN
    SELECT POSITION(',' || STR || ',' IN ',' || STRS || ',') INTO POS;
    IF POS > 0 THEN STATUS = TRUE;
    ELSE STATUS = FALSE;
    END IF;
    RETURN STATUS;
END;
$BODY$ LANGUAGE PLPGSQL;



-- ----------------------------
-- 用来处理 代码生成的视图  仿 mysql的information_schema.tables 和 information_schema.columns
-- ----------------------------

CREATE OR REPLACE view view_self_table
as
select datname                                                   as table_catalog,
       pg_get_userbyid(relowner)                                 AS tableowner,
       nspname                                                   as table_schema,
       relname                                                   as table_name,
       cast(obj_description(relfilenode, 'pg_class') as varchar) as table_comment,
       now()                                                        create_time,
       now()                                                        update_time
from pg_class c
         left join pg_namespace pg_ns on
    pg_ns."oid" = c.relnamespace
         left join pg_database on relowner = datdba
where relname in (select tablename from pg_tables);


CREATE OR REPLACE view view_self_table_columns
as
select table_catalog,
       table_schema,
       table_name,
       ordinal_position                                          as sort,
       column_name,
       data_type                                                 as TypeName,
       (case
            when (is_nullable = 'no' and contype != 'p') then '1'
            else null
           end)                                                  as is_required,
       (case
            when contype = 'p' then '1'
            else '0'
           end)                                                  as is_pk,
       coalesce(character_maximum_length, numeric_precision, -1) as Length,
       numeric_scale                                             as scale,
       case
           is_nullable
           when 'NO' then 0
           else 1
           end                                                   as canNull,
       column_DEFAULT                                            as DEFAULTval,
       case
           when position('nextval' in column_DEFAULT) > 0 then 1
           else 0
           end                                                   as IsIdentity,
       (case
            when position('nextval' in column_DEFAULT) > 0 then 1
            else 0
           end)                                                  as is_increment,
       c.DeText                                                  as column_comment,
       c.typname                                                 as column_type,
       c.contype,
       ordinal_position
from information_schema.columns
         left join (select datname,
                           pg_get_userbyid(relowner) AS tableowner,
                           nspname,
                           relname,
                           attname,
                           description               as DeText,
                           typname,
                           pg_cons.contype
                    from pg_class
                             left join pg_attribute pg_attr on
                        pg_attr.attrelid = pg_class.oid
                             left join pg_description pg_desc on
                            pg_desc.objoid = pg_attr.attrelid
                            and pg_desc.objsubid = pg_attr.attnum
                             left join pg_namespace pg_ns on
                        pg_ns."oid" = pg_class.relnamespace
                             left join pg_database on relowner = datdba
                             left join pg_type on pg_attr.atttypid = pg_type."oid"
                             left join (select pg_con.*, unnest(conkey) conkey_new from pg_constraint pg_con) pg_cons on
                            pg_attr.attrelid = pg_class.oid
                            and pg_attr.attnum = pg_cons.conkey_new and pg_cons.conrelid = pg_class.oid
                    where pg_attr.attnum > 0
                      and pg_attr.attrelid = pg_class.oid
) c
                   on table_catalog = datname and table_schema = nspname and table_name = relname and
                      column_name = attname;


CREATE INDEX dict_type ON sys_dict_type USING btree (
  dict_type  pg_catalog.text_ops ASC NULLS LAST
);

alter sequence if exists sys_user_user_id_seq restart with 10 cache 10;
alter sequence if exists sys_dept_dept_id_seq restart with 200 cache 10;
alter sequence if exists sys_post_post_id_seq restart with 10 cache 10;
alter sequence if exists sys_role_role_id_seq restart with 10 cache 10;
alter sequence if exists sys_menu_menu_id_seq restart with 2000 cache 10;
alter sequence if exists sys_oper_log_oper_id_seq restart with 100 cache 10;
alter sequence if exists sys_dict_type_dict_id_seq restart with 100 cache 10;
alter sequence if exists sys_dict_data_dict_code_seq restart with 100 cache 10;
alter sequence if exists sys_config_config_id_seq restart with 100 cache 10;
alter sequence if exists sys_notice_notice_id_seq cache 10;
alter sequence if exists sys_job_log_job_log_id_seq cache 10;
alter sequence if exists gen_table_table_id_seq cache 10;
alter sequence if exists gen_table_column_column_id_seq cache 10;
alter sequence if exists sys_logininfor_info_id_seq cache 10;
alter sequence if exists sys_job_job_id_seq cache 10;