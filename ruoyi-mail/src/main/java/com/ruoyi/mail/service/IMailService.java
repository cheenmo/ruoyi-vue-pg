package com.ruoyi.mail.service;


public interface IMailService {

    /**
     * 发送邮件
     * @param sendTo      发送目标
     * @param title       邮件标题
     * @param content     邮件正文
     * @param filePath    附件
     * @return  boolean   是否发送成功
     */
    boolean sendMail(String sendTo, String title, String content, String filePath);


}
